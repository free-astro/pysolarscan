# README (EN)

## SOLARSCAN

*SOLARSCAN** ( **SOLAR** **SCAN** ) is a software for processing the video acquisition
of the sun made with the instrument [Sol'Ex](http://www.astrosurf.com/solex/).

It does three things:

1. Reading of the sequence
2. Reconstruction of the raw solar disk
3. Geometric correction of the solar disk


# LISEZ-MOI (FR)

## SOLARSCAN

*SOLARSCAN** ( **SOLAR** **SCAN** ) est un logiciel de traitement de l'acquisition vid�o
du soleil faite avec l'instrument [Sol'Ex](http://www.astrosurf.com/solex/).

Il fait  3 choses :

1. Lecture de  la s�quence
2. Reconstitution du disque brute
3. Correction g�om�trique du disque solaire


________________________________________________________________________________
tested with:

* Python 3.10
* module dependencies :

    - serfilesreader
    - scipy
    - matplotlib
    - lsq_ellipse
    - wxpython
* install modules :
   
    - python -m pip install serfilesreader scipy matplotlib lsq_ellipse wxpython

* run in gui mode: 
    
    - python solarscan.pyw
        
* run in cli mode: 
    
    - python solarscan-cli.py 
    - python solarscan-cli.py <videofile (*.ser)>
________________________________________________________________________________
    