xgettext --language=Python --keyword=_ --output=./solarscan/i18n/solarscan.pot --from-code=UTF-8 ./solarscan/*.pyw ./solarscan/lib/*.py
msginit --input=./solarscan/i18n/solarscan.pot --output=./solarscan/i18n/fr_FR/LC_MESSAGES/solarscan.po

xgettext --language=Python --keyword=_  --output=./solarscan/i18n/solarscan.pot --from-code=UTF-8 ./solarscan/*.py ./solarscan/*.pyw ./solarscan/lib/*.py ./solarscan/ui/gui.py
msgmerge --update --no-fuzzy-matching --backup=off ./solarscan/i18n/fr_FR/LC_MESSAGES/solarscan.po ./solarscan/i18n/solarscan.pot
msgfmt ./solarscan/i18n/fr_FR/LC_MESSAGES/solarscan.po --output-file ./solarscan/i18n/fr_FR/LC_MESSAGES/solarscan.mo
