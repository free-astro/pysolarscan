import sys
import os
import glob
import shutil

from solarscan.lib import changelog #  @UnresolvedImport

# ------------------------------------------------------------------------------
# clean build directories and files
def clean_build(dirlist=None,filelist=None):
    if dirlist is not None :
        for  dirglb in dirlist :
            for dossier in glob.glob( dirglb ):
                print("* clean:",dossier )
                shutil.rmtree(dossier,ignore_errors=True)
    if filelist is not None :
        for  fileglb in filelist :
            for  fic in glob.glob( fileglb ) :
                print("* clean:",fic )
                try:
                    os.unlink(fic)
                except: # pylint: disable=bare-except
                    pass
# ------------------------------------------------------------------------------
def package_setuptools():
    from setuptools import setup, find_packages
    with open("README.md", "r", encoding="utf-8") as fh:
        long_description = fh.read()

    setup(
        name = "solarscan",
        version = changelog.NO_VERSION,
        author="M27trognondepomme",
        author_email="M27trognondepomme@wordpress.com",
        license='LGPL-v3',
        description="solarscan is a graphical frontend for Sol'Ex video.",
        long_description=long_description,
        long_description_content_type="text/markdown",
        url="https://gitlab.com/free-astro/solarscan.git",
        packages=find_packages(),
        python_requires='>=3.9',
        classifiers=[
            "Programming Language :: Python :: 3",
            "Development Status :: 5 - Production/Stable",
            "Programming Language :: Python :: 3.9",
            "Topic :: Scientific/Engineering :: Astronomy",
            "License :: OSI Approved :: GNU General Public License v3 (GPLv3)"
        ],
        install_requires=[ 'wxPython', 'serfilesreader', 'matplotlib', 'ellipse', 'scipy' ],
        #  'numpy','opencv-python','astropy'
        entry_points={
              #'console_scripts': [ 'solar=solarscan.App:Run', ],
              'gui_scripts': [ 'solarscan=solarscan.App:Run', ],
          },
        data_files=[('share/pixmaps',      ["solarscan/icon/solarscan.png"]),
                    ('share/applications', ["solarscan/ui/solarscan.desktop"]),
                     ],
        include_package_data=True
    )
# ------------------------------------------------------------------------------
def package_PyInstaller():
    import PyInstaller.__main__ # @UnresolvedImport ,  pylint:  disable=import-error
    psrc=os.path.join('.')
    sep=":"
    if sys.platform.startswith('win32'):
        sep=";"

    cfg=[
        '--name=%s-V%s' % ('solarscan', changelog.NO_VERSION) ,
        '--noconfirm',
        '--debug',  'all',
        '--hidden-import', 'solarscan',
        '--onefile',
        '--paths=%s'         % psrc,
        '--paths=%s'         % os.path.join( psrc, "solarscan" ),
        '--paths=%s'         % os.path.join( psrc, "solarscan",'ui' ),
        '--paths=%s'         % os.path.join( psrc, "solarscan",'lib' ),
        #'--paths=%s'         % os.path.join( 'C:/awin/python/Python310/lib/site-packages', 'cv2'),
        '--add-data=%s%s%s/' % (os.path.join( psrc, "solarscan"),sep,'solarscan'),
        '--add-data=%s%s%s/' % (os.path.join( psrc, "solarscan",'i18n'),sep,'i18n'),
        '--add-data=%s%s%s/' % (os.path.join( psrc, "solarscan",'icon'),sep,'icon'),
        '--add-data=%s%s%s'  % (os.path.join( psrc, "solarscan",'icon',"solarscan.png"),sep,'.'),
        '--add-data=%s%s%s'  % (os.path.join( psrc, "solarscan",'icon',"solarscan-nb.png"),sep,'.'),
    ]

    if sys.platform.startswith('win32'):
        cfg.append('--windowed')
        cfg.append('--icon=%s' % os.path.join( psrc, "solarscan",'icon', 'solarscan.ico') )

    if not sys.platform.startswith('win32'):
        cfg.append("--strip")

    cfg.append( os.path.join( psrc, "solarscan",'solarscan.pyw' ) )
    PyInstaller.__main__.run(cfg)

# ------------------------------------------------------------------------------
if len(sys.argv ) == 1 :
    print( "Build Package:" )
    print( "    o exec package :" )
    print( "        > python setup.py exec" )
    print( "")
    print( "    o python source package :" )
    print( "        > python setup.py sdist" )
    print( "")
    print( "    o python (*.whl) package :" )
    print( "        > python setup.py bdist_wheel " )
    print( "")
    print( "    o debian (*.deb) package :" )
    print( "        > python setup.py bdist_deb" )
    print( "")
    exit( 0 )

clean_build( [ 'build','dist','deb_dist', '*egg-info' ], ['*tar.gz','solarscan*.spec' ] )

if sys.argv[1] == "exec" :
    package_PyInstaller()
    clean_build( [ 'build'], ['solarscan*.spec' ] )
else:
    package_setuptools()
    clean_build( [ 'build', '*egg-info' ], ['*tar.gz' ] )

